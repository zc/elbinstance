"""\
Manage ELB registrations for an instance at deployment time.

"""
__docformat__ = "reStructuredText"

import boto
import boto.exception
import errno
import os.path
import requests
import sys
import time
import traceback
import zc.zk


STATE_LOCATION = "var/run/elbinstance.txt"
SERVICE_CHECK_INTERVAL = 2


def get_instance_id():
    r = requests.get("http://169.254.169.254/latest/meta-data/instance-id")
    return r.text


class State(object):

    def __init__(self):
        self.path = os.path.join(os.getenv("TEST_ROOT", "/"), STATE_LOCATION)
        self.state = set()
        try:
            with open(self.path, "r") as f:
                self.state.update(f.read().split())
        except IOError as e:
            if e.errno != errno.ENOENT:
                raise

    def __iter__(self):
        return iter(sorted(self.state))

    def add(self, elb):
        if elb not in self.state:
            self.state.add(elb)
            self._save()

    def remove(self, elb):
        if elb in self.state:
            self.state.remove(elb)
            self._save()

    def _save(self):
        with open(self.path, "w") as f:
            for elb in self:
                f.write(elb + "\n")


def run(update):
    state = State()
    node_path, = sys.argv[1:]
    host_id = get_instance_id()
    try:
        zk = zc.zk.ZK()
        properties = zk.properties(node_path, watch=False)
        elbs = properties["elbs"]
        if isinstance(elbs, basestring):
            elbs = [elbs]
        update(boto.connect_elb(), elbs, host_id, state)
    except SystemExit:
        raise
    except:
        # We died; report & exit with a non-zero return code.  We're
        # using sys.stdout since we're invoked via tools anyway, and it
        # makes testing easier.
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)


def starting_deployments():

    def update(control, elbs, host_id, state):
        # Make sure we're not in the ELBs we want to be in post-deployment:
        for elb in elbs:
            print "Deregistering with", elb
            # This is a desired load balancer; to not exist is a
            # configuration error and causes deployment to fail.
            control.deregister_instances(elb, [host_id])
            state.remove(elb)

        # Remove ourselves from any ELBs we should no longer be in:
        for elb in state:
            print "Deregistering with", elb
            try:
                control.deregister_instances(elb, [host_id])
            except boto.exception.BotoServerError as e:
                if e.error_code == "LoadBalancerNotFound":
                    # We don't want to be in this one, so not an error
                    # that it does not exist; it was removed underneath
                    # us.
                    state.remove(elb)
                else:
                    raise
            else:
                state.remove(elb)

    run(update)


def health_check_timeout(hc):
    return (hc.timeout + hc.interval) * (hc.healthy_threshold + 1)


def ending_deployments():

    def update(control, elbs, host_id, state):
        for elb in elbs:
            print "Registering with", elb
            # This is a desired load balancer; to not exist is a
            # configuration error and causes deployment to fail.
            control.register_instances(elb, [host_id])
            state.add(elb)

        # Wait until we're "in service" for all ELBs we registered with:
        max_wait = max([health_check_timeout(elb.health_check)
                        for elb in control.get_all_load_balancers(elbs)])
        t0 = time.time()
        tmax = t0 + max_wait
        while time.time() < tmax:
            for elb in elbs:
                ih = control.describe_instance_health(elb, [host_id])[0]
                if ih.state != "InService":
                    time.sleep(SERVICE_CHECK_INTERVAL)
                    break
            else:
                break
        else:
            # Timed out.
            delta = time.time() - t0
            print "Not in service after registration (%s seconds)" % delta
            sys.exit(1)
        delta = time.time() - t0
        print "In service after registration (%s seconds)" % delta

    run(update)
