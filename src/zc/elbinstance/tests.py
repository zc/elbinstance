"""\
Test harness for zc.elbinstance.

"""
__docformat__ = "reStructuredText"

import doctest
import manuel.capture
import manuel.doctest
import manuel.testing
import mock
import os
import unittest
import zc.elbinstance
import zc.zk.testing
import zope.testing.setupstack


class FauxConnection(object):

    error = None

    def __init__(self):
        self.__elbs = set()
        self.elb_states = {}

    def register_instances(self, elb, host_ids):
        self.__maybe_error()
        print "register_instances(%r, %r)" % (elb, host_ids)
        self.__elbs.add(elb)

    def deregister_instances(self, elb, host_ids):
        self.__maybe_error()
        print "deregister_instances(%r, %r)" % (elb, host_ids)
        if elb in self.__elbs:
            self.__elbs.remove(elb)

    def describe_instance_health(self, elb, instances):
        host_id, = instances
        states = self.elb_states.get(elb, [])
        state = states.pop(0) if states else u"InService"
        return [FauxInstanceState(elb, host_id, state)]

    def get_all_load_balancers(self, elbs):
        return [FauxELB(elb) for elb in elbs]

    def __maybe_error(self):
        if self.error is not None:
            try:
                raise self.error
            finally:
                self.error = None


class FauxELB(object):

    class health_check(object):

        # These times won't be this low, but we don't want to wait for tests.
        interval = 0.2
        timeout = 0.1
        healthy_threshold = 2

    def __init__(self, name):
        self.name = name


class FauxInstanceState(object):

    noisy = False

    def __init__(self, elb, host_id, state):
        self.instance_id = host_id
        self.state = state
        if self.noisy:
            print "INSTANCE HEALTH:", elb, host_id, self.state


def setup(test):
    zope.testing.setupstack.setUpDirectory(test)
    os.makedirs('TEST_ROOT/etc/zmh')
    os.makedirs('TEST_ROOT/var/run')
    os.chdir('TEST_ROOT')
    os.environ['TEST_ROOT'] = os.getcwd()
    get_instance_id = zc.elbinstance.get_instance_id
    zope.testing.setupstack.register(
        test, setattr, zc.elbinstance, "get_instance_id", get_instance_id)
    zc.elbinstance.get_instance_id = lambda: "x-422442"
    connection_string = "zookeeper:2181"
    os.environ["ZC_ZK_CONNECTION_STRING"] = connection_string
    faux_elb_connection = FauxConnection()
    test.globs["faux_elb_connection"] = faux_elb_connection
    zc.zk.testing.setUp(test, "/hosts", connection_string=connection_string)
    zope.testing.setupstack.register(
        test, lambda : zc.zk.testing.tearDown(test))
    interval = zc.elbinstance.SERVICE_CHECK_INTERVAL
    zope.testing.setupstack.register(
        test, lambda : setattr(zc.elbinstance, "SERVICE_CHECK_INTERVAL",
                               interval))
    zc.elbinstance.SERVICE_CHECK_INTERVAL = 0.1
    zope.testing.setupstack.register(
        test, lambda : setattr(FauxInstanceState, "noisy", False))
    def sideeffects():
        while True:
            yield faux_elb_connection
    zope.testing.setupstack.context_manager(
        test, mock.patch("boto.connect_elb", side_effect=sideeffects()))


def test_suite():
    m = manuel.doctest.Manuel(
        optionflags=(doctest.ELLIPSIS | doctest.NORMALIZE_WHITESPACE),
        ) + manuel.capture.Manuel()
    suite = unittest.TestSuite([
        manuel.testing.TestSuite(
            m,
            "README.rst",
            setUp=setup,
            tearDown=zope.testing.setupstack.tearDown,
            ),
        ])
    return suite
