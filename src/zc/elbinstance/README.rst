=========================================
Managing ELB registration for an instance
=========================================

When deploying software to an instance registered with Elastic Load
Balancers, incoming requests can be incorrectly handled because some
components be down even though the front-end server (usually nginx for
us) is up and accepting requests.  To avoid this, we want to remove the
instance from any ELBs it's registered with before we start deployments,
and only restore the instance to the ELBs once deployments have
completed successfully.

This controller needs to know which ELBs an instance should be
registered with; this information is stored in ZooKeeper.  Let's
look at a typical configuration showing this:

    /roles
      /app.customer
        elbs = ["customer-ELB-18T7OL74GIJWT"]

.. -> tree

    >>> import pkg_resources
    >>> import sys
    >>> import zc.zk

    >>> zk = zc.zk.ZK()
    >>> zk.import_tree(tree, trim=True)

    >>> def run(name, *args):
    ...     ep = pkg_resources.load_entry_point(
    ...         "zc.elbinstance", "console_scripts", name)
    ...     oldargs = sys.argv
    ...     sys.argv = [name] + list(args)
    ...     try:
    ...         try:
    ...             ep()
    ...         finally:
    ...             sys.argv = oldargs
    ...     except SystemExit as e:
    ...         rc = e.code
    ...     else:
    ...         rc = 0
    ...     return rc

When the deployment agent is ready to begin deployments, it runs the
``starting-deployments`` script:

    >>> rc = run("starting-deployments", "/roles/app.customer")
    Deregistering with customer-ELB-18T7OL74GIJWT
    deregister_instances(u'customer-ELB-18T7OL74GIJWT', ['x-422442'])

    >>> rc
    0

Since the instance was not already registered with the ELB, we can see
that the de-registration does not fail.

After successful deployments, the ``ending-deployments`` script is run,
with no extra arguments:

    >>> rc = run("ending-deployments", "/roles/app.customer")
    Registering with customer-ELB-18T7OL74GIJWT
    register_instances(u'customer-ELB-18T7OL74GIJWT', ['x-422442'])
    In service after registration (... seconds)

    >>> rc
    0

A file containing the set of ELBs the instance has registered itself
with is maintained in the run directory:

    >>> print open("var/run/elbinstance.txt").read()
    customer-ELB-18T7OL74GIJWT

Now that we're actually registered, we can see that starting deployment
again, causing de-registration, works out as well:

    >>> rc = run("starting-deployments", "/roles/app.customer")
    Deregistering with customer-ELB-18T7OL74GIJWT
    deregister_instances(u'customer-ELB-18T7OL74GIJWT', ['x-422442'])

    >>> rc
    0

    >>> print open("var/run/elbinstance.txt").read()

Let's re-register so we can take a look at some error handling:

    >>> rc = run("ending-deployments", "/roles/app.customer")
    Registering with customer-ELB-18T7OL74GIJWT
    register_instances(u'customer-ELB-18T7OL74GIJWT', ['x-422442'])
    In service after registration (... seconds)

    >>> rc
    0

    >>> print open("var/run/elbinstance.txt").read()
    customer-ELB-18T7OL74GIJWT

If there's an error during registration or deregistration, a return code
of 1 will be returned:

    >>> faux_elb_connection.error = RuntimeError("this is bad")
    >>> rc = run("starting-deployments", "/roles/app.customer")
    Deregistering with customer-ELB-18T7OL74GIJWT
    Traceback (most recent call last):
      ...
    RuntimeError: this is bad

    >>> rc
    1

    >>> faux_elb_connection.error = RuntimeError("we're down now")
    >>> rc = run("ending-deployments", "/roles/app.customer")
    Registering with customer-ELB-18T7OL74GIJWT
    Traceback (most recent call last):
      ...
    RuntimeError: we're down now

    >>> rc
    1

If the configuration specifies an ELB that doesn't exist, the ELB API
will report an error, and deployment will fail with this response::

    <ErrorResponse
        xmlns="http://elasticloadbalancing.amazonaws.com/doc/2012-06-01/"
        >
      <Error>
        <Type>Sender</Type>
        <Code>LoadBalancerNotFound</Code>
        <Message>There is no ACTIVE Load Balancer named 'bad-test-elb'</Message>
      </Error>
      <RequestId>54b83fee-5895-11e4-a1e5-47f2f2a6f64a</RequestId>
    </ErrorResponse>

.. -> error_response

    >>> import boto.exception

    >>> faux_elb_connection.error = boto.exception.BotoServerError(
    ...     400, "Bad Request", error_response)
    >>> rc = run("starting-deployments", "/roles/app.customer")
    Deregistering with customer-ELB-18T7OL74GIJWT
    Traceback (most recent call last):
      ...
    BotoServerError: BotoServerError: 400 Bad Request
    ...

    >>> rc
    1

This would also be an error at the end of deployment, when registering
the target ELBs::

    <ErrorResponse
        xmlns="http://elasticloadbalancing.amazonaws.com/doc/2012-06-01/"
        >
      <Error>
        <Type>Sender</Type>
        <Code>LoadBalancerNotFound</Code>
        <Message>There is no ACTIVE Load Balancer named 'bad-test-elb'</Message>
      </Error>
      <RequestId>54b83fee-5895-11e4-a1e5-47f2f2a6f64a</RequestId>
    </ErrorResponse>

.. -> error_response

    >>> faux_elb_connection.error = boto.exception.BotoServerError(
    ...     400, "Bad Request", error_response)
    >>> rc = run("ending-deployments", "/roles/app.customer")
    Registering with customer-ELB-18T7OL74GIJWT
    Traceback (most recent call last):
      ...
    BotoServerError: BotoServerError: 400 Bad Request
    ...

    >>> rc
    1


Changing the ELBs for a role
----------------------------

When changing the set of ELBs role instances are registered to, any ELBs
that the instances were previously registered with will be dropped if
they are no longer part of the configuration.  Let's update the set of
ELBs to include two new ELBs, but not the previous ELB:

    /roles
      /app.customer
        elbs = ["customer-ELB-1234567890", "customer-ELB-0987654321"]

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> rc = run("starting-deployments", "/roles/app.customer")
    Deregistering with customer-ELB-1234567890
    deregister_instances(u'customer-ELB-1234567890', ['x-422442'])
    Deregistering with customer-ELB-0987654321
    deregister_instances(u'customer-ELB-0987654321', ['x-422442'])
    Deregistering with customer-ELB-18T7OL74GIJWT
    deregister_instances('customer-ELB-18T7OL74GIJWT', ['x-422442'])

    >>> rc
    0

    >>> print open("var/run/elbinstance.txt").read()


Waiting for instance to be in-service
-------------------------------------

Instances need to be considered in-service by the ELB before deployment
is complete.  Above, we've assumed that instances are in-service as soon
as they've been registered, but that's not true in practice.

To deal with this, the ``ending-deployments`` script waits until each of
the ELBs specified in the configuration consider the instance to be back
in service.

Let's register with two ELBs:

    /roles
      /app.customer
        elbs = ["ELB-1", "ELB-2"]

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> import zc.elbinstance.tests
    >>> zc.elbinstance.tests.FauxInstanceState.noisy = True

We'll also arrange for the second ELB to report "out of service" the
first time we check:

    >>> faux_elb_connection.elb_states = {
    ...     "ELB-2": ["OutOfService"],
    ...     }

    >>> rc = run("ending-deployments", "/roles/app.customer")
    Registering with ELB-1
    register_instances(u'ELB-1', ['x-422442'])
    Registering with ELB-2
    register_instances(u'ELB-2', ['x-422442'])
    INSTANCE HEALTH: ELB-1 x-422442 InService
    INSTANCE HEALTH: ELB-2 x-422442 OutOfService
    INSTANCE HEALTH: ELB-1 x-422442 InService
    INSTANCE HEALTH: ELB-2 x-422442 InService
    In service after registration (... seconds)

To determine that a deployment is complete and the instance is
in-service, all ELBs must report InService during a single cycle; ELBs
are not removed from status checks simply because they reported
InService once.  During each check cycle, checks are skipped once any
ELB reports anything other than in-service:

    >>> faux_elb_connection.elb_states = {
    ...     "ELB-1": ["OutOfService", "InService", "OuyOfService", "InService"],
    ...     "ELB-2": ["OutOfService"] * 3,
    ...     }

    >>> rc = run("ending-deployments", "/roles/app.customer")
    Registering with ELB-1
    register_instances(u'ELB-1', ['x-422442'])
    Registering with ELB-2
    register_instances(u'ELB-2', ['x-422442'])
    INSTANCE HEALTH: ELB-1 x-422442 OutOfService
    INSTANCE HEALTH: ELB-1 x-422442 InService
    INSTANCE HEALTH: ELB-2 x-422442 OutOfService
    INSTANCE HEALTH: ELB-1 x-422442 OuyOfService
    INSTANCE HEALTH: ELB-1 x-422442 InService
    INSTANCE HEALTH: ELB-2 x-422442 OutOfService
    INSTANCE HEALTH: ELB-1 x-422442 InService
    INSTANCE HEALTH: ELB-2 x-422442 OutOfService
    INSTANCE HEALTH: ELB-1 x-422442 InService
    INSTANCE HEALTH: ELB-2 x-422442 InService
    In service after registration (... seconds)

In case there's an instance that doesn't come in-service within the time
configured as a healthy measure for the configured ELBs, the
``ending-deployments`` script will eventually time out:

    >>> class always_out_of_service(list):
    ...     def pop(self, index):
    ...         return "OutOfService"

    >>> faux_elb_connection.elb_states = {
    ...     "ELB-1": always_out_of_service(["OutOfService"]),
    ...     }

    >>> rc = run("ending-deployments", "/roles/app.customer")
    Registering with ELB-1
    register_instances(u'ELB-1', ['x-422442'])
    Registering with ELB-2
    register_instances(u'ELB-2', ['x-422442'])
    INSTANCE HEALTH: ELB-1 x-422442 OutOfService
    ...
    INSTANCE HEALTH: ELB-1 x-422442 OutOfService
    Not in service after registration (... seconds)

    >>> rc
    1
