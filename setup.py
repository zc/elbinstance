##############################################################################
#
# Copyright (c) Zope Corporation and Contributors.
# All Rights Reserved.
#
# This software is subject to the provisions of the Zope Public License,
# Version 2.1 (ZPL).  A copy of the ZPL should accompany this distribution.
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY AND ALL EXPRESS OR IMPLIED
# WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF TITLE, MERCHANTABILITY, AGAINST INFRINGEMENT, AND FITNESS
# FOR A PARTICULAR PURPOSE.
#
##############################################################################

import os
import setuptools

entry_points = """
[console_scripts]
starting-deployments = zc.elbinstance:starting_deployments
ending-deployments = zc.elbinstance:ending_deployments
"""

extras_require = {
    "test": [
        "manuel",
        "mock",
        "zope.testing",
        ],
    }

setuptools.setup(
    name="zc.elbinstance",
    version="0",
    author="Fred Drake",
    author_email="fred@zope.com",
    description="Role controller for ELB instances",
    license="ZPL 2.1",
    packages=setuptools.find_packages("src"),
    namespace_packages=["zc"],
    package_dir={"": "src"},
    install_requires=[
        "boto",
        "requests",
        "setuptools",
        "zc.zk",
        ],
    zip_safe=False,
    entry_points=entry_points,
    extras_require=extras_require,
    )
