============================
ELB instance role controller
============================

Role controller for **zkdeployment** that unregisters an EC2 instance
from specified ELBs while deployments are occurring on a machine.


EC2 API access
--------------

This package needs access to several permissions, and has been tested
with an IAM instance role providing access to this policy statement::

    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Sid": "Stmt1405350219000",
          "Effect": "Allow",
          "Action": [
            "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
            "elasticloadbalancing:DescribeInstanceHealth",
            "elasticloadbalancing:DescribeLoadBalancers",
            "elasticloadbalancing:RegisterInstancesWithLoadBalancer"
          ],
          "Resource": [
            "*"
          ]
        }
      ]
    }


Release history
---------------

1.0.1 (2014-10-21)
~~~~~~~~~~~~~~~~~~

Don't declare the deployment complete until configured ELBs show the
instance as in-service.


1.0.0 (2014-10-20)
~~~~~~~~~~~~~~~~~~

Initial release.
